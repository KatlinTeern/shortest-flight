# Find shortest flight path between two airports

This application searches for the shortest path between two airports that consists at most 4 flights and uses Dijkstra's algorithm. 
In addition, it provides suggested route change if there is an airport close to a layover stop. Currently set to 100km radius.

![](image.png)

## Data set
OpenFlights database is used for flight connections. Direct url for connecting flight dataset: [https://raw.githubusercontent.com/jpatokal/openflights/master/data/routes.dat](https://raw.githubusercontent.com/jpatokal/openflights/master/data/routes.dat)

`airport-codes-updated` npm package is used to get airport specific data.

## Start application

    npm install
    cd frontend
    npm install
    cd..
    npm run dev

Go to http://localhost:3000