import React, { useState } from 'react';
import './css/App.css';

function App() {

  const GET = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
  };

  const [notFound, setNotFound] = useState<string>("");
  const [route, setRoute] = useState<string>("");
  const [routeWithChanges, setRouteWithChanges] = useState<string>("");
  const [departure, setDeparture] = useState<string>("");
  const [destination, setDestination] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);

  const getData = () => {
    try {
      if (window.confirm("Are you sure you want to load the latest data from openflights?" + 
            "\nThis will take some time and keep other actions pending while done.")) {
        fetch("/api/routes/create", GET)
      }        
    } catch (e) {
      console.log(e);
    }
  }

  const calculateRoute = async (e: any) => {
    e.preventDefault();
    resetData();
    setLoading(true);

    try {
      const fetchResponse = await fetch("/api/routes/path?src=" + departure + "&dst=" + destination, GET);
      const resp = await fetchResponse.json();

      resp && setLoading(false);
      const shortestRoute = resp.data.flightRoutes.shortestRoute;

      if (shortestRoute.length === 0) {
        setNotFound("Route not found");
      } else {
        const shortestRouteWithChanges = resp.data.flightRoutes.shortestRouteWithChanges;

        if (shortestRouteWithChanges.length !== 0) {
          setRouteWithChanges(formatRouteWithChanges(shortestRouteWithChanges));
        } else {
          setRoute(shortestRoute.join(" -> "));
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  const formatRouteWithChanges = (shortestRouteWithChanges: any[]) => {
    let formatted: string = "";
    shortestRouteWithChanges.forEach((route: []) => {
      formatted = formatted === "" ? formatted + route.join(" -> ") : formatted + " => " + route.join(" -> ");
    })
    return formatted;
  }

  const reset = () => {
    resetData();
    setDeparture("");
    setDestination("");
  }

  const resetData = () => {
    setNotFound("");
    setRoute("");
    setRouteWithChanges("");
  }

  return (
    <div>
      <div className="search">
        <div>Enter airport codes to find the shortest route:</div>
        <br />
        <form onSubmit={calculateRoute}>
          <input
            name="departure"
            type="departure"
            value={departure}
            placeholder="DEPARTURE"
            onChange={e => setDeparture(e.target.value)}
            required />
          <input
            name="destination"
            type="destination"
            value={destination}
            placeholder="DESTINATION"
            onChange={e => setDestination(e.target.value)}
            required />
          <button className="btn-search" type="submit">Calculate route</button>
          <button className="btn-search" onClick={reset}>Clear</button>
        </form>
      </div>
      {loading && (<div className="loading">Loading...</div>)}
      <div className="data">{route}</div>
      <div className="data">{routeWithChanges}</div>
      <div className="not-found">{notFound}</div>
      <div className="footer">
        <button className="btn-data" onClick={getData}>Update data</button>
      </div>

    </div>

  );
}

export default App;

