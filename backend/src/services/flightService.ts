import dijkstra from 'dijkstrajs';

import {
  getAirportsData, getCountryByIata,
  getAirportsByCountry, getDistsanceBetweenTwoAirports,
  getRoutesGraph,
  checkRouteLength,
  calculateTotalDistance
} from '../helpers/FlightHelper';

export const getShortestRoutePath = (from: string, to: string, maxFlights: number) => {
  let routes = [];
  const maxAirports = maxFlights + 1;

  try {
    routes = dijkstra.find_path(getRoutesGraph(getAirportsData()), from, to)
  } catch (e) {}

  if (!routes || !checkRouteLength(routes, maxAirports)) {
    return [];
  }

  return {
    shortestRoute: routes,
    shortestRouteWithChanges: getShortestRouteWithChanges(routes, maxAirports, from, to)
  };
}

const getShortestRouteWithChanges = (routes: string[], maxAirports: number, from: string, to: string) => {
  if(routes.length == 2) {
    return [];
  }

  let totalDistance: number = calculateTotalDistance(routes);
  let possibleRouteChanges: any[] = [];
  const foundAirports: string[] = routes.slice(0, -1);
  const airportsBeforeChange: string[] = [];

  foundAirports.forEach((foundAirport, index) => {
    airportsBeforeChange.push(foundAirport);
    
    if(index > 0) {
      findCloseByAirports(foundAirport).forEach((closeByAirport) => {
        let newRoute: string[] = null;
        try {
          newRoute = dijkstra.find_path(getRoutesGraph(getAirportsData()), closeByAirport, to);
        } catch (e) { }
        
        if (newRoute) {
          const fullNewRoute = airportsBeforeChange.concat(newRoute);
          const newDistance =  calculateTotalDistance(fullNewRoute);
  
          if (checkRouteLength(fullNewRoute, maxAirports + 1) && newDistance < totalDistance) {
            possibleRouteChanges = [airportsBeforeChange, newRoute];
            totalDistance = newDistance;
          }
        } 
      })
    }       
  })
  return possibleRouteChanges;
}

const findCloseByAirports = (iataStart: string) => {
  let closeByAirports: string[] = [];
  const distanceAllowed: number = 100;
  const countryAirports: string[] = getAirportsByCountry(getCountryByIata(iataStart));  

  countryAirports.forEach((iataEnd) => {
    const distance = getDistsanceBetweenTwoAirports(iataStart, iataEnd);    
    if (iataStart !== iataEnd && distance <= distanceAllowed) {
      closeByAirports.push(iataEnd);
    }
  })
  return closeByAirports;
}





