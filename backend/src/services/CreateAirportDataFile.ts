import request from 'request'
import csv from 'csv-parser'
import { getLongLat, getDistanceBetweenGeoPoints, getAirportsDataFilePath } from '../helpers/FlightHelper'
import fs from 'fs';
import { Response } from 'express-serve-static-core';

const CreateAirportDataFile = (res: Response) => {

  let dataJSON = []
  const dataFilePath = getAirportsDataFilePath()

  request('https://raw.githubusercontent.com/jpatokal/openflights/master/data/routes.dat')
    .pipe(csv({
        headers: [
            'airline',
            'airline_id',
            'src',
            'src_id',
            'dst',
            'dst_id',
            'codeshare',
            'stops',
            'equipment'
        ]
    }))
    .on('data', (row) => {
        const srcAirportGeoPoints = getLongLat(row.src)
        const dstAirportGeoPoints = getLongLat(row.dst)

        if (srcAirportGeoPoints && dstAirportGeoPoints) {
          dataJSON.push({
            src: row.src,
            dst: row.dst,
            dist: getDistanceBetweenGeoPoints(srcAirportGeoPoints, dstAirportGeoPoints)
          });
        }

    })
    .on('end', () => {
      fs.writeFileSync(dataFilePath, JSON.stringify(dataJSON));
    })
    .on('error', function(e) {
      console.error('Bad data', e);
    })

    res.status(200).send({
      response: 'New routes data being generated'
    });
}

export default CreateAirportDataFile;
