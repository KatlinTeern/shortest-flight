import express from 'express';
import cors from 'cors';
const app = express();

import bodyParser from 'body-parser';
import fs from 'fs';

app.use(cors())
app.use(express.static(process.cwd() + '/frontend/dist'));

require('./app/routes/index.js')(app, fs);

export default app;
