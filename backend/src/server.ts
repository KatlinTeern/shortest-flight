import express from "express";
import getFlightController from "./controllers/FlightController";
import CreateAirportDataFile from "./services/CreateAirportDataFile";

const PORT = process.env.PORT || 5000;

const app = express();

app.use(express.json());
app.use(express.static("../frontend/build"));

app.listen(PORT, () => {
   console.log(`Server listening on ${PORT}`);
});

const maxFlights: number = 4;

app.get("/api/routes/create", async (req, res) => {
  CreateAirportDataFile(res);
})  

app.get("/api/routes/path", async (req, res) => {
  const flightRoutes = getFlightController(req, res, maxFlights); 
  
  res.status(200).send({
    response: 'OK',
    data: {
      flightRoutes
    }
  });
})  
