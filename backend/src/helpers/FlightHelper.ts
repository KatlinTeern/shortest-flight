import fs from 'fs'
import path from 'path'
import airports from 'airport-codes-updated'
import GeoPoint from 'geopoint'

export interface LongLat {
  latitude: number; 
  longitude: number; 
}

export const getLongLat = (iata: string) => {
  if (airports.findWhere({ iata: iata })) {
    return {
      latitude: Number(airports.findWhere({ iata: iata }).get('latitude')),
      longitude: Number(airports.findWhere({ iata: iata }).get('longitude'))
    }
  }
  return null;
}

export const getDistanceBetweenGeoPoints = (geoPointStart: LongLat, geoPointEnd: LongLat) => {
  const startPoint = new GeoPoint(geoPointStart.latitude, geoPointStart.longitude);
  const endPoint = new GeoPoint(geoPointEnd.latitude, geoPointEnd.longitude);
  return Number(startPoint.distanceTo(endPoint, true).toFixed(2));
}

export const getDistsanceBetweenTwoAirports = (iataStart: string, iataEnd: string) => {
  return Number(getDistanceBetweenGeoPoints(getLongLat(iataStart), getLongLat(iataEnd)));
}

export const getAirportsDataFilePath = () => {
  return path.join(process.cwd(), 'backend/src/data/airportRoutesData.json');
}

export const getAirportsData = () => {
  return JSON.parse(fs.readFileSync(getAirportsDataFilePath()).toString());
}

export const getAirportsListByCountry = () => {
  const airportsJSON = airports.toJSON();
  let airportsByCountry = {};

  for (const k in airportsJSON) {
    const country = airportsJSON[k].country;
    const iata = airportsJSON[k].iata;

    if (iata.length) {
      if (!airportsByCountry[country]) airportsByCountry[country] = []
      if (iata !== '\\N') {
        airportsByCountry[country].push(iata);
      }
    }
  }
  return airportsByCountry;
}

export const getAirportsByCountry = (country: string) => {
  return getAirportsListByCountry()[country];
}

export const getCountryByIata = (iata: string) => {
  const airports = getAirportsListByCountry();
  for (const k in airports) {
    if (airports[k].includes(iata)) {
      return k;
    }
  }
  return null;
}

export const getRoutesGraph = (routesData: any[]) => {
  let routesGraph = {}

  for (let i = 0; i < routesData.length; i++) {
    const src: string = routesData[i].src;
    const dst: string = routesData[i].dst;
    const dist: number = routesData[i].dist;

    if (dist > 0) {
      if (!routesGraph[src]) routesGraph[src] = {}
      routesGraph[src][dst] = dist;
    }
  }
  return routesGraph;
}

export const checkRouteLength = (routes: any[], max: number) => {
  return routes.length <= max;
}

export const calculateTotalDistance = (routes: string[]) => {
  let distance: number = 0;
  for (var i = 0; i < routes.length - 1; i++) {
    distance = distance + getDistsanceBetweenTwoAirports(routes[i], routes[i + 1]);
  }
  return distance;
}