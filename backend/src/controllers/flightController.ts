import { Request, Response } from "express-serve-static-core";
import { getShortestRoutePath } from "../services/FlightService";

const getFlightController = (req: Request, res: Response, maxFlights: number) => {
  const from: any = req.query.src; 
  const to: any = req.query.dst;
  return getShortestRoutePath(from, to, maxFlights);
};

export default getFlightController;